﻿/* Jordan Poertner
 * Assignment 1-6
 * 8/26/2016
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace Chapter_1_6
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Output #1");
            Write("\t Believe in the me that believes in you.");

            WriteLine();
            WriteLine();

            WriteLine("Output #2");
            Write("\t Believe in");
            Write("\n \t the me ");
            Write("\n \t that believes");
            Write("\n \t in you.");

            WriteLine();
            WriteLine();

            WriteLine("Output #3");
            Write("\t Believe");
            Write("\n \t in");
            Write("\n \t the");
            Write("\n \t me");
            Write("\n \t that");
            Write("\n \t believes");
            Write("\n \t in");
            Write("\n \t you.");

            ReadKey();
        }
    }
}
